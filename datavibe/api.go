package datavibe

import "os"
import "time"

import "github.com/jinzhu/gorm"
import _ "github.com/jinzhu/gorm/dialects/sqlite" // required for orm

import "github.com/rs/zerolog"
import "github.com/rs/zerolog/log"

import "github.com/mattn/go-isatty"

func APIEntry(version string, buildarch string) int {
	a := new(API)
	a.version = version
	a.buildarch = buildarch
	a.setupLogging()
	return a.runForever()
}

// Feta is the main structure/process of this app
type API struct {
	version   string
	buildarch string
	//api       *Server
	db        *gorm.DB
	startup   time.Time
}

func (a *API) identify() {
	log.Info().
		Str("version", a.version).
		Str("buildarch", a.buildarch).
		Msg("starting")
}

func (a *API) setupLogging() {

	log.Logger = log.With().Caller().Logger()

	tty := isatty.IsTerminal(os.Stdin.Fd()) || isatty.IsCygwinTerminal(os.Stdin.Fd())

	if tty {
		out := zerolog.NewConsoleWriter(
			func(w *zerolog.ConsoleWriter) {
				// Customize time format
				w.TimeFormat = time.RFC3339
			},
		)
		log.Logger = log.Output(out)
	}

	// always log in UTC
	zerolog.TimestampFunc = func() time.Time {
		return time.Now().UTC()
	}

	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if os.Getenv("DEBUG") != "" {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	a.identify()
}

func (a *API) uptime() time.Duration {
	return time.Since(a.startup)
}

/*
func (f *Feta) setupDatabase() {
	var err error
	f.db, err = gorm.Open("sqlite3", "feta.sqlite")

	if err != nil {
		panic(err)
	}

	//f.databaseMigrations()
}
*/

func (a *API) runForever() int {
	a.startup = time.Now()

	//f.setupDatabase()

	home := os.Getenv("HOME")
	if home == "" {
		panic("can't find home directory")
	}

	// FIXME(sneak)
	for {
		time.Sleep(1 * time.Second)
	}

	return 0
}
