module git.eeqj.de/datavibe/api

go 1.13

require (
	github.com/jinzhu/gorm v1.9.12
	github.com/mattn/go-isatty v0.0.12
	github.com/rs/zerolog v1.18.0
)
