package main

import "os"

import "git.eeqj.de/datavibe/api/datavibe"

// these are filled in at link-time by the build scripts

// Version is the git version of the app
var Version string

// Buildarch contains the architecture it is compiled for
var Buildarch string

func main() {
	os.Exit(datavibe.APIEntry(Version, Buildarch))
}
