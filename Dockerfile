FROM golang:1.13 as builder

WORKDIR /go/src/git.eeqj.de/datavibe/api
COPY . .

#RUN make lint && make build
RUN make build

WORKDIR /go
RUN tar cvfz go-src.tgz src && du -sh *

# this container doesn't do anything except hold the build artifact
# and make sure it compiles.

FROM alpine

COPY --from=builder /go/src/git.eeqj.de/datavibe/api/api /bin/api

# put the source in there too for safekeeping
COPY --from=builder /go/go-src.tgz /usr/local/src/go-src.tgz

CMD /bin/api

# FIXME add testing
